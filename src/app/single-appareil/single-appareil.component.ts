import { Component } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-single-appareil',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './single-appareil.component.html',
  styleUrl: './single-appareil.component.scss',
})
export class SingleAppareilComponent {
  name: string | undefined;
  status: string | undefined;

  constructor(
    private appareilService: AppareilService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.params['id'];
    const appareil = this.appareilService.getAppareilById(id);
    if(appareil) {
      this.name = appareil.name;
      this.status = appareil.status;
    }
  }
}
