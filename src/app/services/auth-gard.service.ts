import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from "@angular/router";
import { AuthService } from "./auth.service";

export const AuthGard : CanActivateFn = (route: ActivatedRouteSnapshot, state : RouterStateSnapshot ) => {
    const service = inject(AuthService);
    const router = inject(Router);

    if(service.isAuth) {
        return true;
    } else {
        alert('Acces denied')
        router.navigate(['/auth'])
        return false
    }

}