import { Subject } from "rxjs";
import { User } from "../models/User.model";

export class UserService {
    private users : User[] = [
        {
            firstName: 'james',
            lastName : 'Smith',
            email : 'james@smith.com',
            foodpreference : 'pates',
            hobbies: [
                'coder',
                'Netflix'
            ]
        }
    ]
    userSubject = new Subject<User[]>();

    emitUsers() {
        this.userSubject.next(this.users.slice());
    }

    addUser(user : User) {
        this.users.push(user);
        this.emitUsers();
    }
}