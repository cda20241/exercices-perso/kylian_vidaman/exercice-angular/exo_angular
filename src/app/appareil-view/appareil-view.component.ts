import { AsyncPipe, DatePipe, NgFor, UpperCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppareilComponent } from '../appareil/appareil.component';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil-view',
  standalone: true,
  imports: [AppareilComponent, NgFor, DatePipe, UpperCasePipe, AsyncPipe],
  templateUrl: './appareil-view.component.html',
  styleUrl: './appareil-view.component.scss',
})
export class AppareilViewComponent implements OnInit {
  isAuth = false;

  lastUpdate = new Date();

  appareils: any[] | undefined;
  appareilSubscription!: Subscription

  constructor(private appareilService: AppareilService) {
    setTimeout(() => {
      this.isAuth = true;
    }, 4000);
  }

  ngOnInit(): void {
    this.appareilSubscription = this.appareilService.appareilSubject.subscribe(
      (appareils: any[]) => {
        this.appareils =appareils;
      }
    );
    this.appareilService.emitAppareilSubject();
  }

  onAllumer() {
    this.appareilService.switchOnAll();
  }

  onEteindre() {
    this.appareilService.switchOffAll();
  }
}
