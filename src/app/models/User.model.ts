export class User {
  constructor(
    public firstName: string,
    public lastName: string,
    public email: string,
    public foodpreference: string,
    public hobbies?: string[]
  ) {}
}
