import { NgClass, NgIf, NgStyle } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil',
  standalone: true,
  imports: [FormsModule, NgIf, NgStyle, NgClass, RouterLink],
  templateUrl: './appareil.component.html',
  styleUrl: './appareil.component.scss',
})
export class AppareilComponent {
  @Input()
  appareilName!: string;
  @Input()
  appareilStatus!: string;
  @Input()
  indexOfAppareil!: number;
  @Input()
  id!: number;

  constructor(private appareilService: AppareilService) {}

  ngOnInit() {}

  getStatus() {
    return this.appareilStatus;
  }

  getColor() {
    if (this.appareilStatus === 'allumé') {
      return 'green';
    } else {
      return 'red';
    }
  }

  onSwitchOn() {
    this.appareilService.switchOnOne(this.indexOfAppareil);
  }

  onSwitchOff() {
    this.appareilService.switchOffOne(this.indexOfAppareil);
  }
}
