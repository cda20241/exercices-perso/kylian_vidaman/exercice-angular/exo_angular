import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { Subscription, interval } from 'rxjs';
import { AppareilViewComponent } from './appareil-view/appareil-view.component';
@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  imports: [
    RouterOutlet,
    AppareilViewComponent,
    RouterLink,
    RouterLinkActive,
    ReactiveFormsModule,
  ],
})
export class AppComponent implements OnInit {
  secondes!: number;
  counterSubscription!: Subscription;

  constructor() { /* TODO document why this constructor is empty */ }

  ngOnInit(): void {
    const counter = interval(1000);
    this.counterSubscription = counter.subscribe((value: number) => {
      this.secondes = value;
    });
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }
}
