import { Routes } from '@angular/router';
import { AppareilViewComponent } from './appareil-view/appareil-view.component';
import { AuthComponent } from './auth/auth.component';
import { EditAppareilComponent } from './edit-appareil/edit-appareil.component';
import { FourOfFourComponent } from './four-of-four/four-of-four.component';
import { NewUserComponent } from './new-user/new-user.component';
import { AuthGard } from './services/auth-gard.service';
import { SingleAppareilComponent } from './single-appareil/single-appareil.component';
import { UserListComponent } from './user-list/user-list.component';

export const routes: Routes = [
  {
    path: 'appareils',
    component: AppareilViewComponent,
    canActivate: [AuthGard],
  },
  {
    path: 'appareils/:id',
    component: SingleAppareilComponent,
    canActivate: [AuthGard],
  },
  { path: 'edit', component: EditAppareilComponent, canActivate: [AuthGard] },
  { path: 'auth', component: AuthComponent },
  { path: 'users', component: UserListComponent},
  { path: 'new-user', component: NewUserComponent},
  { path: '', component: AppareilViewComponent },
  { path: 'not-found', component: FourOfFourComponent },
  { path: '**', redirectTo: '/not-found' },
];
