import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { routes } from './app.routes';
import { AppareilService } from './services/appareil.service';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes), AppareilService, AuthService, UserService]
};
